const { fork } = require('child_process');
let response = {success: 0, error: 0};
let count = process.argv[2];

if (count === undefined) {
    console.log("Please add the number of processes that should be run");
    process.exit(1);
}


function runTest() {
    
    const test = fork('./test.js');
    test.on('exit', function(code) { 
        if(code === 0) {
            response.success = response.success + 1;
        } else {
            response.error = response.error + 1;
        } 
        console.log("Succes: " + response.success + " - Error: " + response.error);
    });
  }
  
  for(var i = 0; i < count; i++) {
      runTest();
  }

  //console.log("Succes: " + response.success + " - Error: " + response.error);