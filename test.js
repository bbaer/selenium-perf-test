
const chrome = require('selenium-webdriver/chrome');
const {Builder, By, Key, until} = require('selenium-webdriver');

const screen = {
    width: 1920,
    height: 1080
  };

// console.log('Setting PATH');
process.env.PATH = process.env.PATH + ":" + process.env.PWD;
 
(async function example() {
  
  let driver = await new Builder().forBrowser('chrome')
    .setChromeOptions(new chrome.Options().headless().windowSize(screen).addArguments('--incognito'))
    .build();
  try {
    await driver.get('https://www.google.com/ncr');
    await driver.findElement(By.name('q')).sendKeys('webdriver', Key.RETURN);
    await driver.wait(until.titleIs('webdriver - Google Search'), 1000);
  } finally {
    await driver.quit();
  }
})();